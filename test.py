from SEMOpt.model import Model
from pandas import read_csv
from pandas import DataFrame as df
from time import time
from SEMOpt.optimizer import Optimizer as CO
from SEMOpt.stats import gather_statistics
from SEMOpt.visualisation import visualise
from SEMOpt.inspector import inspect

directory = 'testingData/models'
filename_model = 'mod1.txt'
filename_data = 'data1.txt'
import numpy as np
with open(directory + '/' + filename_model) as f:
    mod = f.read()


data = read_csv(directory + '/' + filename_data, engine='c', na_filter=False,
                index_col=None, low_memory=False)

m = Model(mod)
m.load_dataset(data)
visualise(m)

t = time()
opt = CO(m)
print(opt.optimize(method='SLSQP'))
print((time() - t) / 1)
beta = df(opt.mx_beta, index=m.beta_names[0], columns=m.beta_names[1])
lamb = df(opt.mx_lambda, index=m.lambda_names[0], columns=m.lambda_names[1])
psi = df(opt.mx_psi, index=m.psi_names[0], columns=m.psi_names[1])
thet = df(opt.mx_theta, index=m.theta_names[0], columns=m.theta_names[1])
inspect(opt)

    
#n =2000
#t = time()
#for i in range(n):
#    w = m.calculate_sigma_gradient(m.mx_beta, m.mx_lambda, m.mx_psi, m.mx_theta)
#print("Cython:", (time() - t) / n)
#t = time()
#for i in range(n):
#    w = m.calculate_sigma_gradient2(m.mx_beta, m.mx_lambda, m.mx_psi, m.mx_theta)
#print("Cython:", (time() - t) / n)
#t = time()
#for i in range(n):
#    sigma = m.calculate_sigma_gradient2(m.mx_beta, m.mx_lambda, m.mx_psi, m.mx_theta)
#print("Python:", (time() - t) / n)
